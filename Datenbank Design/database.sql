CREATE TABLE `Books` (
  `book_id` INT PRIMARY KEY,
  `titel` VARCHAR(255),
  `author` VARCHAR(255),
  `available` BOOLEAN
);

CREATE TABLE `Users` (
  `user_id` INT PRIMARY KEY,
  `name` VARCHAR(255),
  `email` VARCHAR(255),
  `payment_method` VARCHAR(50)
);

CREATE TABLE `Licences` (
  `license_id` INT PRIMARY KEY,
  `book_id` INT,
  `count` INT
);

CREATE TABLE `Orders` (
  `order_id` INT PRIMARY KEY,
  `user_id` INT,
  `book_id` INT,
  `order_date` DATE
);

CREATE TABLE `Payments` (
  `payment_id` INT PRIMARY KEY,
  `user_id` INT,
  `amount` DECIMAL(10, 2),
  `payment_date` DATE
);

CREATE TABLE `Reminders` (
  `reminder_id` INT PRIMARY KEY,
  `user_id` INT,
  `message` TEXT,
  `reminder_date` DATE
);

ALTER TABLE `Licences` ADD FOREIGN KEY (`book_id`) REFERENCES `Books` (`book_id`);

ALTER TABLE `Orders` ADD FOREIGN KEY (`user_id`) REFERENCES `Users` (`user_id`);

ALTER TABLE `Orders` ADD FOREIGN KEY (`book_id`) REFERENCES `Books` (`book_id`);

ALTER TABLE `Payments` ADD FOREIGN KEY (`user_id`) REFERENCES `Users` (`user_id`);

ALTER TABLE `Reminders` ADD FOREIGN KEY (`user_id`) REFERENCES `Users` (`user_id`);
