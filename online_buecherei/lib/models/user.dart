import 'package:online_buecherei/models/book.dart';
import 'package:online_buecherei/models/payment_method.dart';

class User {
  late String _id;
  late String _username;
  late String? _password;
  late String _email;
  late PaymentMethod _paymentMethod;
  late List<Book> _borrowedBooks;

  User.withPassword(String id, String username, String password, String email,
      PaymentMethod paymentMethod, List<Book> borrowedBooks) {
    _id = id;
    _username = username;
    _password = password;
    _email = email;
    _paymentMethod = paymentMethod;
    _borrowedBooks = borrowedBooks;
  }

  User.noPassword(String id, String username, String email,
      PaymentMethod paymentMethod, List<Book> borrowedBooks) {
    _id = id;
    _username = username;
    _email = email;
    _paymentMethod = paymentMethod;
    _borrowedBooks = borrowedBooks;
  }

  String get id => _id;
  String get username => _username;
  String? get password => _password;
  String get email => _email;
  PaymentMethod? get paymentMethod => _paymentMethod;
  List<Book>? get borrowedBooks => _borrowedBooks;
}
