class Book {
  late String _id;
  late String _title;
  late String _author;
  late String _content;
  late double _rentPrice;
  Book(String id, String title, String author, String content,
      double rentPrice) {
    _id = id;
    _title = title;
    _author = author;
    _content = content;
    _rentPrice = rentPrice;
  }

  String get id => _id;
  String get title => _title;
  String get author => _author;
  String get content => _content;
  double get rentPrice => _rentPrice;
}
