import 'package:online_buecherei_flutter/app/models/book.dart';
import 'package:online_buecherei_flutter/app/models/user.dart';
import 'package:online_buecherei_flutter/app/systems/database/database.dart';

/// A class responsible for managing the ordering of books in the online library system.
class OrderingSystem {
  late Database _db;

  /// Constructs an [OrderingSystem] instance with the provided [Database] instance.
  ///
  /// Parameters:
  ///   - instance: The [Database] instance to be used for ordering books.
  OrderingSystem(Database instance) {
    _db = instance;
  }

  /// Orders a book for the specified user.
  ///
  /// Parameters:
  ///   - user: The user who is ordering the book.
  ///   - book: The book to be ordered.
  ///
  /// Returns:
  ///   True if the book is successfully ordered for the user, otherwise false.
  bool orderBook(User user, Book book) {
    return _db.giveBookToUser(user, book);
  }
}
