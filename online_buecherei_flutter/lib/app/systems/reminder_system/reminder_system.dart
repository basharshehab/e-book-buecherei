import 'package:online_buecherei_flutter/app/models/book.dart';
import 'package:online_buecherei_flutter/app/models/user.dart';
import 'package:online_buecherei_flutter/app/systems/database/database.dart';
import 'package:online_buecherei_flutter/app/systems/payment_system/payment_system.dart';

/// A class responsible for managing reminders and fines related to book
/// returns.
class ReminderSystem {
  late Database _db;

  /// Constructs a [ReminderSystem] instance with the provided [Database]
  /// instance.
  ///
  /// Parameters:
  ///   - instance: The [Database] instance to be used for reminder operations.
  ReminderSystem(Database instance) {
    _db = instance;
  }

  /// Handles the process of returning a book, including checking for late
  ///  returns and applying fines if necessary.
  ///
  /// Parameters:
  ///   - user: The user who is returning the book.
  ///   - book: The book being returned.
  ///   - paymentSystem: The payment system instance for handling fines.
  void returnBook(User user, Book book, PaymentSystem paymentSystem) {
    // query database with user_id, book_id
    Duration durationSinceBorrowed = _db.durationSinceBorrowed(user, book);

    bool returnedOnTime = durationSinceBorrowed.inDays > 28;

    if (returnedOnTime == false) {
      double amount =
          _fineUser(book, user, durationSinceBorrowed, paymentSystem);
      _db.markFineInUserAccount(user, book, amount);
    }
  }

  /// Calculates the fine amount for a user based on the book and late duration
  /// using the provided [PaymentSystem].
  ///
  /// Parameters:
  ///   - book: The book for which the fine is being calculated.
  ///   - user: The user who is being fined.
  ///   - lateDuration: The duration for which the book is late.
  ///   - paymentSystem: The [PaymentSystem] instance used to calculate fines.
  ///
  /// Returns:
  ///   The calculated fine amount.
  double _fineUser(Book book, User user, Duration lateDuration,
      PaymentSystem paymentSystem) {
    return paymentSystem.fineUser(user, book, lateDuration);
  }

  /// Sends a reminder message to the user using the provided message.
  ///
  /// Parameters:
  ///   - user: The user to whom the reminder message will be sent.
  ///   - message: The message content of the reminder.
  ///
  /// Returns:
  ///   `true` if the reminder is successfully sent; otherwise, `false`.
  bool sendReminderToUser(User user, String message) {
    // sends a reminder to contact information
    // provided by user. e. g Email Address.
    // return true on success
    return true;
  }
}
