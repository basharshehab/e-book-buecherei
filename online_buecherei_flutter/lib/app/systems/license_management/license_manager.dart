import 'package:online_buecherei_flutter/app/models/book.dart';
import 'package:online_buecherei_flutter/app/systems/database/database.dart';

/// A class responsible for managing licenses for books in the online library system.
class LicenseManager {
  late Database _db;

  /// Constructs a [LicenseManager] instance with the provided [Database] instance.
  ///
  /// Parameters:
  ///   - instance: The [Database] instance to be used for license management.
  LicenseManager(Database instance) {
    _db = instance;
  }

  /// Checks if a license is available for the specified book.
  ///
  /// Parameters:
  ///   - book: The book for which the availability of the license will be checked.
  ///
  /// Returns:
  ///   True if a license is available for the book, otherwise false.
  bool isBookAvailable(Book book) {
    // check if we have a license for this book.
    return true;
  }

  /// Requests new licenses for the specified book from regional license providers.
  ///
  /// Parameters:
  ///   - book: The book for which new licenses will be requested.
  void requestNewLicences(Book book) {
    // request new licenses from regional license providers.
  }

  /// Decrements the available licenses count for the specified book.
  ///
  /// Parameters:
  ///   - book: The book for which the available licenses count will be decremented.
  void decrementLicencesCount(Book book) {
    _db.decrementLicencesCount(book);
  }

  /// Increments the available licenses count for the specified book.
  ///
  /// Parameters:
  ///   - book: The book for which the available licenses count will be incremented.
  void incrementLicencesCount(Book book) {
    _db.incrementLicencesCount(book);
  }
}
