import 'dart:async';

import 'package:online_buecherei_flutter/app/models/bill.dart';
import 'package:online_buecherei_flutter/app/models/book.dart';
import 'package:online_buecherei_flutter/app/models/user.dart';
import 'package:online_buecherei_flutter/app/systems/database/database.dart';

/// A class responsible for managing payments and fines in the online library system.
class PaymentSystem {
  late Database _db;

  /// Constructs a [PaymentSystem] instance with the provided [Database] instance.
  ///
  /// Parameters:
  ///   - instance: The [Database] instance to be used for payment operations.
  PaymentSystem(Database instance) {
    _db = instance;
  }

  /// Calculates the fine amount for a user based on the book and late duration.
  ///
  /// Parameters:
  ///   - user: The user who is being fined.
  ///   - book: The book for which the fine is being calculated.
  ///   - lateDuration: The duration for which the book is late.
  ///
  /// Returns:
  ///   The calculated fine amount.
  double fineUser(User user, Book book, Duration lateDuration) {
    // multiplicator to increment the fine accourding to different variables.
    double multiplicator = 1;
    // check how many reminders have been sent to user already
    // fine will increment by 10% for each reminder sent and ignored
    int remindersCount = _db.fetchUserRemindersCount(user);
    multiplicator += 0.1 * remindersCount;

    // ignore: unused_local_variable
    double finalFineAmount = book.rentPrice * multiplicator;
    Bill bill = Bill(
      "${user.username}${book.id}${lateDuration.inSeconds}",
      user.username,
      finalFineAmount,
      DateTime.now().add(
        const Duration(days: 7),
      ),
    );
    _checkWeekly(bill, user);
    // send a fine with this amount to the user.
    return finalFineAmount;
  }

  /// Increases the bill amount by the specified increment.
  ///
  /// Parameters:
  ///   - currentBillAmount: The current amount of the bill.
  ///   - increment: The amount by which to increment the bill.
  ///
  /// Returns:
  ///   The updated bill amount.
  double _increaseBillAmount(double currentBillAmount, double multiplicator) {
    return currentBillAmount * multiplicator;
  }

  /// Checks the user's fines weekly and updates the bill amount if necessary.
  ///
  /// Parameters:
  ///   - db: The [Database] instance to perform database operations.
  ///   - bill: The bill to be updated.
  ///   - user: The user whose bill is being checked.
  void _checkWeekly(Bill bill, User user) {
    Timer.periodic(const Duration(days: 7), (Timer t) {
      bool res = _db.checkUserFinesWeeklyPaid(user);
      if (res) {
        return;
      }
      double newAmount = _increaseBillAmount(bill.amount, 1.15);
      bill.updateAmount(newAmount);
    });
  }
}
