import 'package:get/get.dart';

import '../modules/einstellungen/bindings/einstellungen_binding.dart';
import '../modules/einstellungen/views/einstellungen_view.dart';
import '../modules/start_seite/bindings/start_seite_binding.dart';
import '../modules/start_seite/views/start_seite_view.dart';

// ignore_for_file: constant_identifier_names

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.START_SEITE;

  static final routes = [
    GetPage(
      name: _Paths.START_SEITE,
      page: () => const StartSeiteView(),
      binding: StartSeiteBinding(),
    ),
    GetPage(
      name: _Paths.EINSTELLUNGEN,
      page: () => const EinstellungenView(),
      binding: EinstellungenBinding(),
    ),
  ];
}
