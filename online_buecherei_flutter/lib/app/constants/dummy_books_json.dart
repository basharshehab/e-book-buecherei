List<Map<String, dynamic>> books = [
  {
    "id": "9781593279509",
    "title": "Eloquent JavaScript, Third Edition",
    "author": "Marijn Haverbeke",
    "price": "472",
    "content": "http://eloquentjavascript.net/"
  },
  {
    "id": "9781491943533",
    "title": "Practical Modern JavaScript",
    "author": "Nicolás Bevacqua",
    "price": "334",
    "content": "https://github.com/mjavascript/practical-modern-javascript"
  },
  {
    "id": "9781593277574",
    "title": "Understanding ECMAScript 6",
    "author": "Nicholas C. Zakas",
    "price": "352",
    "content": "https://leanpub.com/understandinges6/read"
  },
  {
    "id": "9781449365035",
    "title": "Speaking JavaScript",
    "author": "Axel Rauschmayer",
    "price": "460",
    "content": "http://speakingjs.com/"
  },
  {
    "id": "9781449331818",
    "title": "Learning JavaScript Design Patterns",
    "author": "Addy Osmani",
    "price": "254",
    "content":
        "http://www.addyosmani.com/resources/essentialjsdesignpatterns/book/"
  },
  {
    "id": "9798602477429",
    "title": "You Don't Know JS Yet",
    "author": "Kyle Simpson",
    "price": "143",
    "content":
        "https://github.com/getify/You-Dont-Know-JS/tree/2nd-ed/get-started"
  },
  {
    "id": "9781484200766",
    "title": "Pro Git",
    "author": "Scott Chacon and Ben Straub",
    "price": "458",
    "content": "https://git-scm.com/book/en/v2"
  },
  {
    "id": "9781484242216",
    "title": "Rethinking Productivity in Software Engineering",
    "author": "Caitlin Sadowski, Thomas Zimmermann",
    "price": "310",
    "content": "https://doi.org/10.1007/978-1-4842-4221-6"
  }
];
