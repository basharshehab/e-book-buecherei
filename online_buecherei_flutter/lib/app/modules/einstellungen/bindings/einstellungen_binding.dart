import 'package:get/get.dart';

import '../controllers/einstellungen_controller.dart';

class EinstellungenBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<EinstellungenController>(
      () => EinstellungenController(),
    );
  }
}
