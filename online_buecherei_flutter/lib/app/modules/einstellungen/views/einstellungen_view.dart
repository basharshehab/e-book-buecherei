import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/einstellungen_controller.dart';

class EinstellungenView extends GetView<EinstellungenController> {
  const EinstellungenView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('EinstellungenView'),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextButton(
              onPressed: () {},
              child: const Text("Zahlungsmethode eintragen"),
            ),
            TextButton(
              onPressed: () {},
              child: const Text(
                "Ausloggen",
                style: TextStyle(color: Colors.red),
              ),
            ),
            TextButton(
              onPressed: () {},
              child: const Text(
                "Konto löschen",
                style: TextStyle(color: Colors.red),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
