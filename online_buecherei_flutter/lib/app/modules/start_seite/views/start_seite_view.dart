import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:online_buecherei_flutter/app/routes/app_pages.dart';

import '../controllers/start_seite_controller.dart';

class StartSeiteView extends GetView<StartSeiteController> {
  const StartSeiteView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: MyAppBar(),
        body: GridView.builder(
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 4,
          ),
          itemBuilder: (ctx, index) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: ClipRRect(
                    child: Image.network(
                      "https://media.istockphoto.com/id/647809752/de/foto/alte-b%C3%BCcher-mit-vintage-bindungen-und-wundersch%C3%B6ne-vergoldete-leder-buchumschl%C3%A4ge.jpg?s=612x612&w=is&k=20&c=p0RRXbCR4eB-y4qSi_TrTZcltj1PfT1IbPxEKzZbPK8=",
                      fit: BoxFit.cover, // Adjust the fit as needed
                    ),
                  ),
                ),
                Text(
                  controller.allAvailableBooks[index]
                      .title, // Add your desired text here
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                ),
                GetBuilder<StartSeiteController>(
                  id: "borrowBook",
                  builder: (_) => controller.allAvailableBooks[index].isLent
                      ? TextButton(
                          onPressed: () => controller
                              .returnBook(controller.allAvailableBooks[index]),
                          child: const Text("Buch zurückgeben"),
                        )
                      : TextButton(
                          onPressed: () => controller
                              .borrowBook(controller.allAvailableBooks[index]),
                          child: const Text("Buch ausleihen"),
                        ),
                ),
              ],
            );
          },
          itemCount: controller.allAvailableBooks.length,
        ));
  }
}

class MyAppBar extends StatelessWidget implements PreferredSize {
  const MyAppBar({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: const Text('Online-Bücherei'),
      centerTitle: true,
      leading: const CircleAvatar(
        foregroundImage: NetworkImage(
            "https://images.pexels.com/photos/3763188/pexels-photo-3763188.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"),
      ),
      actions: [
        IconButton(
          onPressed: () {
            Get.toNamed(Routes.EINSTELLUNGEN);
          },
          icon: const Icon(Icons.settings),
        ),
      ],
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);

  @override
  // TODO: implement child
  Widget get child => const SizedBox();
}
