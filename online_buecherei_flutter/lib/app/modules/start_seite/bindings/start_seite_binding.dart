import 'package:get/get.dart';

import '../controllers/start_seite_controller.dart';

class StartSeiteBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<StartSeiteController>(
      () => StartSeiteController(),
    );
  }
}
