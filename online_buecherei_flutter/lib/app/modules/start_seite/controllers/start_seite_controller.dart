import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:online_buecherei_flutter/app/models/book.dart';
import 'package:online_buecherei_flutter/app/models/payment_method.dart';
import 'dart:async';

import 'package:online_buecherei_flutter/app/models/user.dart';
import 'package:online_buecherei_flutter/app/systems/database/database.dart';
import 'package:online_buecherei_flutter/app/systems/license_management/license_manager.dart';
import 'package:online_buecherei_flutter/app/systems/ordering_system/ordering_system.dart';
import 'package:online_buecherei_flutter/app/systems/payment_system/payment_system.dart';
import 'package:online_buecherei_flutter/app/systems/reminder_system/reminder_system.dart';

class StartSeiteController extends GetxController {
  //TODO: Implement StartSeiteController
  late User _user;
  List<Book> allAvailableBooks = [];
  late PaymentSystem _paymentSystem;
  late LicenseManager _licenseManagement;
  late OrderingSystem _orderingSystem;
  late ReminderSystem _reminderSystem;
  late Database _db;

  /// initializes the [OnlineBuecherei] controller by performing the following steps:
  /// 1. Retrieves the list of all available books using the private method [_getAvailableBooks].
  /// 2. Retrieves user data using the private method [_getUserData].
  /// 3. Initializes a new instance of the [Database] class and assigns it to the [_db] property.
  /// 4. Initializes a new instance of the [PaymentSystem] class, passing the database instance [_db].
  ///    The resulting instance is assigned to the [_paymentSystem] property.
  /// 5. Initializes a new instance of the [LicenseManager] class, passing the database instance [_db].
  ///    The resulting instance is assigned to the [_licenseManagement] property.
  /// 6. Initializes a new instance of the [OrderingSystem] class, passing the database instance [_db].
  ///    The resulting instance is assigned to the [_orderingSystem] property.
  /// 7. Initializes a new instance of the [ReminderSystem] class, passing the database instance [_db].
  ///    The resulting instance is assigned to the [_reminderSystem] property.
  /// 8. Sets up a periodic timer to run every 30 days, performing the following actions:
  ///    a. Checks if all user payments are monthly paid by calling [_db.checkUserPaymentsMonthlyAllPaid].
  ///    b. If payments are all paid, the timer callback returns.
  ///    c. Otherwise, sends a reminder to the user using [_reminderSystem.sendReminderToUser] and marks it in the database.
  ///    d. Retrieves the count of reminders for the user using [_db.fetchUserRemindersCount].
  ///    e. If the reminder count is less than 2, the timer callback returns.
  ///    f. Otherwise, informs the internal department for escalation using [_informInternalDepartment].
  ///
  /// Note: This constructor sets up automated reminders and escalations for users with unpaid monthly payments.
  /// The timer ensures that these checks are performed periodically.
  @override
  void onInit() {
    super.onInit();
    _db = Database();

    allAvailableBooks = _getAvailableBooks();
    _user = _getUserData();
    _paymentSystem = PaymentSystem(_db);
    _licenseManagement = LicenseManager(_db);
    _orderingSystem = OrderingSystem(_db);
    _reminderSystem = ReminderSystem(_db);

    Timer.periodic(const Duration(days: 30), (Timer t) {
      bool res = _db.checkUserPaymentsMonthlyAllPaid(_user);
      if (res == true) {
        return;
      }
      _reminderSystem.sendReminderToUser(_user, "Mahnung");
      _db.markReminderInUserAccount(_user, "Mahnung");
      int reminderCount = _db.fetchUserRemindersCount(_user);
      if (reminderCount < 2) {
        return;
      }
      _informInternalDepartment(_user, reminderCount, "Eskalation");
    });
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  /// Retrieves user data from the database [currently creates dummy data].
  ///
  /// This method simulates retrieving user data from the database. If the user data is not found,
  /// it creates dummy data for the user. The dummy data includes a cached user ID, username, email,
  /// payment method, and an empty list of borrowed books.
  ///
  /// Returns:
  ///   - [User]: The user object containing the retrieved or dummy user data.
  ///
  User _getUserData() {
    // get userdata from database, or from account creation
    // dummy data, as if we called userdata from database.
    // this ID should be cached and called from cache, and if it exists, then we
    // call user data from database.
    // If not found, then there is no current user account,
    // prompt user to login or signin.
    String cachedDummyId = "id";

    String dummyUsername = "username";
    String dummyEmail = "email";
    PaymentMethod dummyPaymentMethod = PaymentMethod.BANK_TRANSFER;
    List<Book> dummyBorrowedBooks = [];
    // initialise User object
    return User.noPassword(
      cachedDummyId,
      dummyUsername,
      dummyEmail,
      dummyPaymentMethod,
      dummyBorrowedBooks,
    );
  }

  /// Retrieves a list of available books.
  ///
  /// This method returns a list of available books from the database.
  ///
  /// Returns:
  ///   - List<Book>: A list of available books.
  List<Book> _getAvailableBooks() {
    List<Book> availableBooks = _db.getAvailableBooks();
    update([ValueKey("availableBooks")]);
    return availableBooks;
  }

  /// Borrows a book for the current user.
  ///
  /// This method attempts to borrow a book for the current user from the library.
  /// It checks if a license is available for the specified book, and if so, proceeds
  /// with the borrowing process. If no license is available, it requests a new batch
  /// of licenses for the book.
  ///
  /// Parameters:
  ///   - book: The book to be borrowed.
  ///
  /// Returns:
  ///   - bool: True if the book was successfully borrowed, false otherwise.
  bool borrowBook(Book book) {
    int i = allAvailableBooks.indexWhere((element) => element == book);
    allAvailableBooks[i].isLent = true;
    update(["borrowBook"]);

    // API call with book_id and user_id
    // check if a license is available
    // if available, continue.
    bool avail = _licenseManagement.isBookAvailable(book);
    // load book in app
    // update amount of available licenses.
    if (avail) {
      _orderingSystem.orderBook(_user, book);
      _licenseManagement.decrementLicencesCount(book);
      // app will automatically retrieve books
      // assigned to the user from the database.
      return true;
    } else {
      // no license, request a new batch of licenses
      _requestNewLicenses(book);
      return false;
    }
  }

  /// Returns a borrowed book to the library.
  ///
  /// This method is invoked when a user wants to return a borrowed book.
  /// It triggers the reminder system to check if the book has been returned
  /// within 4 weeks and takes necessary actions. Afterward, the book is deleted
  /// from the user's account, and the number of available licenses for the book
  /// is incremented.
  ///
  /// Parameters:
  ///   - book: The book to be returned.
  void returnBook(Book book) {
    int i = allAvailableBooks.indexWhere((element) => element == book);
    allAvailableBooks[i].isLent = false;
    update(["borrowBook"]);

    // "Return Book" button has been clicked
    // ReminderSystem checks if book has been returned within 4 weeks
    // and takes necessary action.
    _reminderSystem.returnBook(_user, book, _paymentSystem);
    // Book will be deleted regardless of latency.
    _deleteBook(_user, book);
    _licenseManagement.incrementLicencesCount(book);
  }

  /// Requests new licenses for a specific book.
  ///
  /// This method is called when there are no available licenses for a book
  /// that a user wants to borrow. It triggers the LicenseManagement system
  /// to request new licenses for the specified book.
  ///
  /// Parameters:
  ///   - book: The book for which new licenses are requested.
  void _requestNewLicenses(Book book) {
    _licenseManagement.requestNewLicences(book);
  }

  /// Deletes a book from the user's account in the database.
  ///
  /// This method removes the specified book from the user's account in the database.
  ///
  /// Parameters:
  ///   - user: The user whose account the book will be deleted from.
  ///   - book: The book to be deleted from the user's account.
  void _deleteBook(User user, Book book) {
    // delete book from user account in database.
    _db.deleteBook(user, book);
  }

  /// Informs an internal department about late payments from a user.
  ///
  /// This method is responsible for notifying an internal department about
  /// late payments from a user, along with the number of reminders sent and
  /// a specific message.
  ///
  /// Parameters:
  ///   - user: The user for whom the late payment is being reported.
  ///   - reminderCount: The number of reminders sent to the user.
  ///   - message: The message indicating the reason for the notification.
  void _informInternalDepartment(User user, int reminderCount, String message) {
    // Logic here to inform an internal department about late payments
    // from this user.
  }
}
