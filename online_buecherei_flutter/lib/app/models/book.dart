class Book {
  late String _id;
  late String _title;
  late String _author;
  late String _content;
  late double _rentPrice;
  late bool _isLent;
  Book(String id, String title, String author, String content, double rentPrice,
      isLent) {
    _id = id;
    _title = title;
    _author = author;
    _content = content;
    _rentPrice = rentPrice;
    _isLent = isLent;
  }

  String get id => _id;
  String get title => _title;
  String get author => _author;
  String get content => _content;
  double get rentPrice => _rentPrice;
  bool get isLent => _isLent;
  set isLent(bool newValue) {
    _isLent = newValue;
  }

  Book.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _title = json['title'];
    _author = json['author'];
    _content = json['content'];
    _rentPrice = double.parse(json['price']);
    _isLent = false;
  }
}
