class Bill {
  late String _id;
  late String _customerName;
  late double _amount;
  late DateTime _dueDate;

  Bill(
    id,
    customerName,
    amount,
    dueDate,
  ) {
    _id = id;
    _customerName = customerName;
    _amount = amount;
    _dueDate = dueDate;
  }
  String get id => _id;
  String get customerName => _customerName;
  double get amount => _amount;
  DateTime get dueDate => _dueDate;
  @override
  String toString() {
    return 'Bill ID: $id, Customer: $customerName, Amount: $amount, Due Date: $dueDate';
  }

  void updateAmount(double newAmount) {
    _amount = newAmount;
  }
}
